# Compile less files whenever any file under css directory changes
while inotifywait -r -e MODIFY css; do lessc --compress css/style.less > css/style.css; done;
